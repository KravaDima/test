<?php

require_once 'vendor/autoload.php';

use App\Entity\Apple\PlasticApple;
use App\Entity\Person\Behavior\Eat;
use App\Entity\Person\Behavior\NoEat;
use App\Entity\Person\RealPerson;
use App\Entity\Apple\GreenApple;
use App\Entity\Person\RubberPerson;

$greenApple = new GreenApple(3);
$plasticApple = new PlasticApple(2);
$eat = new Eat();
$noEat = new NoEat();
$rubberPerson = new RubberPerson('Mike', $eat);
$realPerson = new RealPerson('John', $eat);
$realPerson->takeApple($greenApple);



echo $realPerson->eat();
echo $rubberPerson->eat();

