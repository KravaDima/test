<?php

declare(strict_types=1);

use App\Entity\Apple\GreenApple;
use App\Entity\Person\Behavior\Eat;
use App\Entity\Person\RealPerson;
use PHPUnit\Framework\TestCase;

class RealPersonTest extends TestCase
{
    /**
     * Проверка факта откусывания яблока (некое "изменение состояния")
     * @throws Exception
     */
    public function testEatApple()
    {
        $eat = new Eat();
        $apple = new GreenApple(3);
        $realPerson = new RealPerson('John', $eat);
        $realPerson->takeApple($apple);

        $this->assertTrue($realPerson->eat()->getPiece() === 2);
    }

    /**
     * Проверка факта наличия яблока для откусывания
     * @throws Exception
     */
    public function testAppleExistence()
    {
        $eat = new Eat();
        $apple = new GreenApple(3);
        $realPerson = new RealPerson('John', $eat);
        $realPerson->takeApple($apple);

        $this->assertNotNull($realPerson->getApple());
    }
}