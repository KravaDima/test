<?php

declare(strict_types=1);

use App\Entity\Apple\GreenApple;
use PHPUnit\Framework\TestCase;

class GreenAppleTest extends TestCase
{
    /**
     * Проверка возможности откусывания яблока
     * @throws Exception
     */
    public function testSuccessBite()
    {
        $apple = new GreenApple(3);

        $this->assertTrue($apple->isBite());
    }

    /**
     * Проверка невозможности откусывания яблока
     * @throws Exception
     */
    public function testFailBite()
    {
        $apple = new GreenApple(1);
        $apple->bite();

        $this->assertFalse($apple->isBite());
    }
}