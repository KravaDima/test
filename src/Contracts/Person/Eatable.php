<?php

declare(strict_types=1);

namespace App\Contracts\Person;

use App\Entity\Apple\Apple;

/**
 * Interface Eatable
 */
interface Eatable
{
    /**
     * @param Apple $apple Яблоко.
     *
     * @return Apple
     */
    public function eat(Apple $apple): Apple;
}
