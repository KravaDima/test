<?php

declare(strict_types=1);

namespace App\Entity\Person;

use App\Contracts\Person\Eatable;
use App\Entity\Apple\Apple;

/**
 * Class People
 */
abstract class Person
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Apple
     */
    protected $apple = null;

    /**
     * @var Eatable
     */
    private $eatBehavior;

    /**
     * People constructor.
     *
     * @param string  $name Имя.
     * @param Eatable $eatBehavior
     */
    public function __construct(string $name, Eatable $eatBehavior)
    {
        $this->name = $name;
        $this->eatBehavior = $eatBehavior;
    }

    /**
     * Метод получения имени.
     *
     * @return string
     */
    public  function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Apple $apple
     */
    public function takeApple(Apple $apple): void
    {
        $this->apple = $apple;
    }

    /**
     * @return Apple|null
     */
    public function getApple(): ?Apple
    {
        return $this->apple;
    }

    public function getEatBehavior(): Eatable
    {
        return $this->eatBehavior;
    }

    /**
     * @return Apple
     */
    abstract public function eat(): Apple;
}