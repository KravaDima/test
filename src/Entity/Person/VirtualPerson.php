<?php

declare(strict_types=1);

namespace App\Entity\Person;

use App\Entity\Apple\Apple;

/**
 * Class VirtualPerson
 */
class VirtualPerson extends Person
{
    /**
     * @return Apple
     */
    public function eat(): Apple
    {
        echo 'Chuvk Chuvk';
    }
}