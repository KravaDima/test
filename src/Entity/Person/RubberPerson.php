<?php

declare(strict_types=1);

namespace App\Entity\Person;

use App\Entity\Apple\Apple;

/**
 * Class RubberPerson
 */
class RubberPerson extends Person
{
    /**
     * @return Apple
     */
    public function eat(): Apple
    {
        echo 'I RubberPerson, I can not eat.';
    }
}