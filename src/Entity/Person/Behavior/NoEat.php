<?php

declare(strict_types=1);

namespace App\Entity\Person\Behavior;

use App\Contracts\Person\Eatable;
use App\Entity\Apple\Apple;

/**
 * Class NoEat
 */
class NoEat implements Eatable
{
    /**
     * @param Apple $apple Яблоко.
     *
     * @return Apple
     */
    public function eat(Apple $apple): Apple
    {
        echo 'No Eat ' . $apple->getName();
        return $apple;
    }
}