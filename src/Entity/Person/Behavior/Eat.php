<?php

declare(strict_types=1);

namespace App\Entity\Person\Behavior;

use App\Contracts\Person\Eatable;
use App\Entity\Apple\Apple;

/**
 * Class Eat
 */
class Eat implements Eatable
{
    /**
     * @param Apple $apple Яблоко.
     *
     * @return Apple
     */
    public function eat(Apple $apple): Apple
    {
        if (! $apple->isBite()) {
            throw new \InvalidArgumentException('Яблоко нельза укусить');
        }

        $apple->bite();

        echo 'Eat ' . $apple->getName() . ' : piece - ' . $apple->getPiece();
        return $apple;
    }
}
