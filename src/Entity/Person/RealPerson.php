<?php

declare(strict_types=1);

namespace App\Entity\Person;

use App\Entity\Apple\Apple;

/**
 * Class RealPerson
 */
class RealPerson extends Person
{
    /**
     * @return Apple
     * @throws \Exception
     */
    public function eat(): Apple
    {
        if (! $apple = $this->getApple()) {
            throw new \Exception('Для того, чтоб съесть яблоко сначала возьмите его.');
        }

        return $this->getEatBehavior()->eat($apple);
    }
}