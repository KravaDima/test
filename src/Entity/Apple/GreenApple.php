<?php

declare(strict_types=1);

namespace App\Entity\Apple;

/**
 * Class GreenApple
 */
class GreenApple extends Apple
{
    /**
     * @return string
     */
    function getName(): string
    {
        return 'Green Apple';
    }

    /**
     * @return bool
     */
    function isBite(): bool
    {
        return $this->getPiece() > 1;
    }
}
