<?php

declare(strict_types=1);

namespace App\Entity\Apple;

/**
 * Class PlasticApple
 */
class PlasticApple extends Apple
{
    /**
     * @return string
     */
    function getName(): string
    {
        return 'Plastic Apple';
    }

    /**
     * @return bool
     */
    function isBite(): bool
    {
        return false;
    }
}