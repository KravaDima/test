<?php

declare(strict_types=1);

namespace App\Entity\Apple;

/**
 * Class Apple
 */
abstract class Apple
{
    /**
     * @var int
     */
    private $piece;

    /**
     * Apple constructor.
     *
     * @param int $piece
     */
    public function __construct(int $piece)
    {
        if ($piece < 1) {
            throw new \InvalidArgumentException('Количесвто укусов яблока, должна быть более 0');
        }

        $this->piece = $piece;
    }

    /**
     * @return string
     */
    abstract function getName(): string;

    /**
     * @return int
     *
     */
    public function getPiece(): int
    {
        return $this->piece;
    }

    /**
     * @return bool
     */
    abstract function isBite(): bool;

    /**
     * @return void
     */
    public function bite(): void
    {
        $this->isBite() ? $this->piece-- : null;
    }
}